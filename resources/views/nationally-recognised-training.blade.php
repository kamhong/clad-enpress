@extends('layouts.app')
@section ('content')
  @include('sections.other-hero', [
      'data' => (object)([
        'hero_height' => '546px',
        'hero_title' => 'Nationally Recognised Training',
        'hero_content' => 'Collective Learning and Development offers a wide range of courses to clients.  These courses have been developed through years of consultation with clients and industry bodies.  Our courses are designed to provide ongoing development to leaders of all levels.'
      ])
    ])
    @include('sections.training-tabs')
    <div class="container">
    
    @include('components.card-tab', [
      'data' => (object)([
        'card_id' => 'debt-collection-courses',
        'feature_image' => asset('dist/media/course-images/debt-collection-courses.jpg'),
        'card_title' => 'Debt Collection Courses',
        'card_text' => '<p>Our vast library of debt collection content can be adapted to create a tailored workshop to your business.  Debt collection and overdue account conversation require critical elements for successful outcomes. </p>',
        'left_header' => 'For example',
        'left_content' => '<p>Learning to Leadership</p>
                           <p>Talent Management Basics Performance Management</p>
                           <p>Introduction to Coaching </p>
                           <p>Introduction to Facilitation </p>
                           <p>Business Etiquette </p>
                           <p>Problem Solving and Decision </p>
                           <p>Negotiation Skills </p>
                           <p>Dealing with Difficult Customers </p>
                           <p>Communication Skills </p>
                           <p>Understanding and Assessing Culture </p>' 
      ])
    ])
    @include('components.card-tab', [
      'data' => (object)([
        'card_id' => 'hardship-identification-and-resolution',
        'feature_image' => asset('dist/media/course-images/hardship.jpg'),
        'card_title' => 'Hardship Identification and Resolution',
        'card_text' => '<p>Our hardship courses teach learners how to identify hardship and how to ensure correct action is taken. </p>',
        'left_header' => 'For example',
        'left_content' => '<p>Identifying Hardship</p>
                           <p>Different Types of Hardship</p>
                           <p>Hardship Legislation</p>
                           <p>Personal Obligations</p>' 
      ])
    ])
    @include('components.card-tab', [
      'data' => (object)([
        'card_id' => 'mercantile-courses',
        'feature_image' => asset('dist/media/course-images/mercantile-courses.jpg'),
        'card_title' => 'Mercantile Courses',
        'card_text' => '<p>Our focus is assertive, practical, quality and ethical customer service and collection techniques totally tailored to your needs. </p>',
        'left_header' => 'For example',
        'left_content' => '<p>Negotiation Principles</p>
                           <p>Assertive Debt Conversations</p>
                           <p>Benefits Vs Consequences</p>
                           <p>Profiling the Customer</p>
                           <p>Skip Tracing</p>
                           <p>Debt Collection Legislation Courses</p>' 
      ])
    ])
    @include('components.card-tab', [
        'data' => (object)([
          'card_id' => 'financial-literacy',
          'feature_image' => asset('dist/media/course-images/financial-literacy.jpg'),
          'card_title' => 'Financial Literacy',
          'card_text' => '<p>Our financial literacy courses provide learners with basic financial literacy skills:</p>',
          'left_header' => 'For example',
          'left_content' => '<p>Different Types of Credit</p>
                            <p>Preparing a Budget</p>
                            <p>Managing Debt</p>
                            <p>Bankruptcy</p>
                            <p>Saving</p>
                            <p>Different types of insurance</p>
                            <p>Superannuation</p>' 
        ])
      ])
    @include('components.card-tab', [
        'data' => (object)([
          'card_id' => 'qualifications',
          'feature_image' => asset('dist/media/course-images/qualifications.jpg'),
          'card_title' => 'Qualifications',
          'card_text' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer dictum lectus.</p>',
          'left_header' => 'For example',
          'left_content' => '<p>LSI</p>
                            <p>Disc</p>
                            <p>Five Behaviours</p>' 
        ])
      ])
  </div>
@endsection