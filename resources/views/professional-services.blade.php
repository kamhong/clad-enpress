@extends('layouts.app')
@section ('content')
  @include('sections.other-hero', [
      'data' => (object)([
        'hero_height' => '546px',
        'hero_title' => 'Professional Services',
        'hero_content' => 'Professional Services are designed to provide ongoing development to leaders of all levels.  Collective Learning and Development have an abundance of training on offer which can be tailored to suit the leadership cohort and needs of your business.'
      ])
    ])
  @include('sections.services-tabs')
  <div class="container">
    
    @include('components.card-tab', [
      'data' => (object)([
        'card_id' => 'develop-new-leaders-suite',
        'feature_image' => asset('dist/media/course-images/develop-new-leaders-suite.jpg'),
        'card_title' => 'Develop New Leaders Suite',
        'card_text' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer dictum lectus dignissim, commodo massa sed, scelerisque lorem. Pellentesque volutpat, est nec sollicitudin posuere, risus felis vestibulum sem, nec congue leo lacus ut ligula. Proin facilisis ultrices erat, id eleifend mauris iaculis eu. </p>',
        'left_header' => 'Skills explored',
        'left_content' => '<p>Learning to Leadership</p>
                           <p>Talent Management Basics Performance Management</p>
                           <p>Introduction to Coaching </p>
                           <p>Introduction to Facilitation </p>
                           <p>Business Etiquette </p>
                           <p>Problem Solving and Decision </p>
                           <p>Negotiation Skills </p>
                           <p>Dealing with Difficult Customers </p>
                           <p>Communication Skills </p>
                           <p>Understanding and Assessing Culture </p>' 
      ])
    ])
    @include('components.card-tab', [
      'data' => (object)([
        'card_id' => 'frontline-leaders',
        'feature_image' => asset('dist/media/course-images/frontline-leaders.jpg'),
        'card_title' => 'Frontline Leaders',
        'card_text' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer dictum lectus dignissim, commodo massa sed, scelerisque lorem. Pellentesque volutpat, est nec sollicitudin posuere, risus felis vestibulum sem, nec congue leo lacus ut ligula. Proin facilisis ultrices erat, id eleifend mauris iaculis eu. </p>
                        <p>Pellentesque volutpat, est nec sollicitudin posuere, risus felis vestibulum sem, nec congue leo lacus ut ligula. Proin facilisis ultrices erat, id eleifend mauris iaculis eu. </p>',
        'left_header' => 'Skills explored',
        'left_content' => '<p>Leadership Styles</p>
                           <p>Stress Management and Resilience</p>
                           <p>Advanced Coaching</p>
                           <p>Career Management </p>
                           <p>Time Management</p>
                           <p>Influence and Persuasion </p>
                           <p>Aligning Group Culture </p>
                           <p>Team Leadership </p>
                           <p>Sales Management</p>
                           <p>Advanced Coaching</p>
                           <p>Self-Awareness and Self-Assessment</p>
                           <p>Project Management</p>
                           <p>Managing Change</p>
                           <p>Managing Conflict as a Leader</p>
                           <p>Advanced Negotiation Skills</p>
                           <p>Influence and Persuasion</p>
                           <p>Cross-Cultural Communication – Debt Collection</p>' 
      ])
    ])
    @include('components.card-tab', [
      'data' => (object)([
        'card_id' => 'specialist-accredited-services',
        'feature_image' => asset('dist/media/course-images/specialist-accredited-services.jpg'),
        'card_title' => 'Specialist Accredited Services',
        'card_text' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer dictum lectus dignissim, commodo massa sed, scelerisque lorem. Pellentesque volutpat, est nec sollicitudin posuere, risus felis vestibulum sem, nec congue leo lacus ut ligula. Proin facilisis ultrices erat, id eleifend mauris iaculis eu. </p>',
        'left_header' => 'Skills explored',
        'left_content' => '<p>LSI</p>
                           <p>Disc</p>
                           <p>Five Behaviours </p>' 
      ])
    ])
  </div>
@endsection