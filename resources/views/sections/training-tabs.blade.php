<div class="tab-menu">
  <ul class="list-tab">
    <li>
      <a href="{{ url('/nationally-recognised-training#debt-collection-courses') }}" data-action="smooth-scroll">Debt Collection Courses </a>
    </li>
    <li>
      <a href="{{ url('/nationally-recognised-training#hardship-identification-and-resolution') }}" data-action="smooth-scroll">Hardship Identification and Resolution</a>
    </li>
    <li>
      <a href="{{ url('/nationally-recognised-training#mercantile-courses') }}" data-action="smooth-scroll">Mercantile Courses</a>
    </li>
    <li>
      <a href="{{ url('/nationally-recognised-training#financial-literacy') }}" data-action="smooth-scroll">Financial Literacy</a>
    </li>
    <li>
      <a href="{{ url('/nationally-recognised-training#qualifications') }}" data-action="smooth-scroll">Qualifications</a>
    </li>
  </ul>
</div>