<div class="section">
  <div class="text-center bg-warning py-2 py-md-4">
    <div class="now-call d-flex justify-content-center text-center mx-auto align-items-center">
      @include ('shared.svgs.phone', ['class' => 'svg-icon svg-icon-danger'])
      <div class="d-flex flex-wrap">
        <h5 class="h6 md-h5 ml-2 mb-0">Call us now on 13OO 367 37O</h5>
        <h5 class="h6 md-h5 text-left">&nbsp; or <a class="text-danger" href="/contact"> get in touch</a></h5>
      </div>
    </div>
  </div>
  <div class="section-calendar py-8 py-md-12">
    <div class="container">
      <div class="section-header">
        <h4 class="d-block mb-5 mb-md-0 mr-3">Event &amp; course calendar</h4>
        <a class="btn btn-outline-danger" href="/events">See all events</a>
      </div>
      <app-event-calendar class="event-calendar">
        <div class="event-calendar-main">
          <div class="event-calendar-heading">
            <div class="title"></div>
            <div class="control">
              <div class="prev"></div>
              <div class="next"></div>
            </div>
          </div>
          <div class="event-calendar-table">
          </div>
        </div>
        <div class="event-calendar-content">
        </div>
        <div class="event-calendar-loading">
        </div>
      </app-event-calendar>
    </div>
  </div>
</div>