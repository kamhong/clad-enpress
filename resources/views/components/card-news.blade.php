<div class="col-md-6 col-lg-4 mb-4 d-flex pr-md-2">
  <div class="card-news shadow">
    <div class="news-image" style="background-image: url('{{$data->news_image}}')"></div>
    <div class="card-body pr-md-3 bg-white">
      <p class="font-weight-bold text-info">{{$data->news_date}}</p>
      <h5 class="text-danger mb-3">{{$data->news_headline}}</h5>
      {!!$data->news_content!!}
      <a class="btn btn-sm btn-outline-danger" href="#">Read more</a>
    </div>
  </div>
</div>