<div class="col-lg-6 mb-4 d-flex pr-lg-2">
  <div class="card-services shadow">
    <div class="card-image" style="background-image: url('{{ $data->card_image }}')">
    </div>
    <div class="card-body bg-white">
      <h5 class="text-danger">{{$data->card_title}}</h5>
      {!!$data->card_content!!}
      <a class="btn btn-outline-danger mt-5 mb-3" href="{{$data->link_url}}">Learn more</a>
    </div>
  </div>
</div>