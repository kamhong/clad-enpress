<div class="card-tab py-4 py-md-7" id="{{$data->card_id}}">
  <div class="feature-image" style="background-image: url('{{$data->feature_image}}')"></div>
  <div class="card-content">
    <h4 class="card-title pt-5">{{$data->card_title}}</h4>
    <div class="row">
      <div class="col-md-7 card-text pr-0 pr-md-7">
        {!!$data->card_text!!}
      <div class="d-none d-md-flex">
        <a href="/contact/#contact" class="btn btn-outline-danger btn-sm mt-4 mb-4">Get in touch</a>
      </div>
      </div>
      <div class="col-md-5">
        <div class="card-right-content border-left-secondary pl-7">
          <h5 class="text-info font-weight-medium mb-4">{{$data->left_header}}</h5>
          {!!$data->left_content!!}
        </div>
        <div class="d-flex d-md-none">
          <a href="/contact/#contact" class="btn btn-outline-danger btn-sm mt-4 mb-4">Get in touch</a>
        </div>
      </div>
    </div>
  </div>
</div>