<div class="col-lg-6 mb-4 d-flex pr-md-2">
  <div class="card-team shadow">
    <div class="team-image w-100" style="background-image: url('{{$data->team_image}}')"></div>
    <div class="card-body bg-white">
      <h5 class="text-danger">{{$data->team_name}}</h5>
      <p class="font-weight-bold text-info">{{$data->team_courses}}</p>
      {!!$data->text_content!!}
    </div>
  </div>
</div>