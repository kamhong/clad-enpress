<div class="col-md-6 col-lg-4 mb-4 d-flex pr-md-2">
  <div class="card-event shadow">
    <a href="{{ $data->event_url }}">
      <div class="event-image" style="background-image: url('{{ $data->event_image }}')"></div>
      <div class="card-body pr-md-3 bg-white {{ $data->event_type }}">
        <small class="font-weight-bold">{{ $data->event_date}}</small>
        <h5 class="mb-3">{{ $data->event_title }}</h5>
        {!!$data->event_content!!}
        <p class="state-title font-weight-bold">{{ $data->state_title }}</p>
      </div>
    </a>
  </div>
</div>