<div class="card-icon d-flex justify-content-center">
  <div class="card-content text-center">
    <img src="{{$data->icon_name}}"/>
    <p class="mt-3 font-weight-bold">{{$data->card_text}}</p>
  </div>
</div>