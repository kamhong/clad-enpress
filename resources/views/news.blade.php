@extends('layouts.app')
@section ('content')
  @include('sections.other-hero', [
      'data' => (object)([
        'hero_height' => '450px',
        'hero_title' => 'News'
      ])
    ])
<div class="py-6 py-md-8">
  <div class="container">
    <div class="row">
      @include('components.card-news', [
        'data' => (object)([
          'news_image' => asset('dist/media/course-images/news-image-1.jpg'),
          'news_date' => '10 August 2018',
          'news_headline' => 'News Headline',
          'news_content' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id mattis lacus. Sed varius tristique ipsum vel condimentum. Nulla pulvinar faucibus nisi, a tempor nibh ultrices vel. </p>'
        ])
      ])
      @include('components.card-news', [
        'data' => (object)([
          'news_image' => asset('dist/media/course-images/news-image-4.jpg'),
          'news_date' => '10 August 2018',
          'news_headline' => 'News Headline',
          'news_content' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id mattis lacus. Sed varius tristique ipsum vel condimentum. Nulla pulvinar faucibus nisi, a tempor nibh ultrices vel. </p>'
        ])
      ])
      @include('components.card-news', [
        'data' => (object)([
          'news_image' => asset('dist/media/course-images/news-image-5.jpg'),
          'news_date' => '10 August 2018',
          'news_headline' => 'News Headline',
          'news_content' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id mattis lacus. Sed varius tristique ipsum vel condimentum. Nulla pulvinar faucibus nisi, a tempor nibh ultrices vel. </p>'
        ])
      ])
      @include('components.card-news', [
        'data' => (object)([
          'news_image' => asset('dist/media/course-images/news-image-2.jpg'),
          'news_date' => '10 August 2018',
          'news_headline' => 'News Headline',
          'news_content' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id mattis lacus. Sed varius tristique ipsum vel condimentum. Nulla pulvinar faucibus nisi, a tempor nibh ultrices vel. </p>'
        ])
      ])
      @include('components.card-news', [
        'data' => (object)([
          'news_image' => asset('dist/media/course-images/news-image-3.jpg'),
          'news_date' => '10 August 2018',
          'news_headline' => 'News Headline',
          'news_content' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id mattis lacus. Sed varius tristique ipsum vel condimentum. Nulla pulvinar faucibus nisi, a tempor nibh ultrices vel. </p>'
        ])
      ])
      @include('components.card-news', [
        'data' => (object)([
          'news_image' => asset('dist/media/course-images/news-image-6.jpg'),
          'news_date' => '10 August 2018',
          'news_headline' => 'News Headline',
          'news_content' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id mattis lacus. Sed varius tristique ipsum vel condimentum. Nulla pulvinar faucibus nisi, a tempor nibh ultrices vel. </p>'
        ])
      ])
    </div>
  </div>
</div>
@endsection