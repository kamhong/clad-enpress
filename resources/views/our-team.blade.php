@extends('layouts.app')
@section ('content')
  @include('sections.other-hero', [
      'data' => (object)([
        'hero_height' => '546px',
        'hero_title' => 'Our Team',
        'hero_content' => 'Our learning solutions aim to help develop talent, build skills, knowledge and capability and help support growth of your organisation. Focusing on Adult Learning Principles, we create customised, engaging workshops that speak to all types of learners.'
      ])
    ])
<div class="pt-5 pt-md-11 pb-6 pb-md-14">
  <div class="container">
    <div class="row">
      @include('components.card-team', [
        'data' => (object)([
          'team_image' => asset('dist/media/people/laura.jpg'),
          'team_name' => 'Laura Herman',
          'team_courses' => 'Collective Learning and Development Manager',
          'text_content' => '<p>Leading Collective Learning and Development, Laura Herman is a skilled consultant, facilitator, designer and coach with extensive experience in the Learning & Development, Financial Services, Retail and Customer Service Industries.</p>
                             <p>Laura is a Qualified Certificate IV Trainer – TAE40110 with TAELLN411 accreditation</p>
                             <p class="mb-0">LSI TM Accredited Practitioner</p>
                             <p class="mb-0">DiSC TM Accredited Practitioner</p>
                             <p class="mb-0">5 Behaviours of a Cohesive Team </p>
                             <p class="mb-0">(DiSC TM) Accredited Practitioner</p>'
        ])
      ])
      @include('components.card-team', [
        'data' => (object)([
          'team_image' => asset('dist/media/people/anthony.jpg'),
          'team_name' => 'Anthony Rivas',
          'team_courses' => 'Managing Director and CEO',
          'text_content' => '<p>Mr. Anthony Rivas comes to Collection House Limited with over 25 years’ experience in the area of Credit and Collections, and extensive international experience in three continents. Anthony has served as Managing Director of Australian Receivables Limited since July 2015, after joining the company in 2013 to oversee operations and then being promoted when the Founder and CEO retired.</p>
                             <p>His accomplishments included: ‘ Assisting companies to bring purchased debt portfolios to India for the first time ‘ Vice President of Operations/Training for Global Vantedge (an OSI company) in the USA and India.</p>'
        ])
      ])
    </div>
  </div>
</div>
@endsection
